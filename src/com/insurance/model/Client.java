package com.insurance.model;
import java.util.HashMap;
import java.util.Map;

public class Client {

 String name;
 String gender;
 int  age;
 float premium;
 
 public static final int BASIC_PREMIUM= 5000;

 
 Map health = new HashMap<String,String> ();
 Map habits = new HashMap<String,String>();
 
 
 public Map getHealth() {
	 if(health== null) {
			health = new HashMap<String, String>();
		}
	return health;
}
public void setHealth(Map health) {
	this.health = health;
}
public Map getHabits() {
	if(habits== null) {
		habits = new HashMap<String, String>();
	}
	return habits;
}
public void setHabits(Map habits) {
	this.habits = habits;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public float getPremium() {
	return premium;
}
public void setPremium(float premium) {
	this.premium = premium;
}
public Client() {
	super();
    this.premium = BASIC_PREMIUM;
	// TODO Auto-generated constructor stub
}
 

}

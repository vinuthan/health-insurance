package com.insurance.quoter;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.insurance.model.Client;

public class InsuranceQuoter {
	

	public static final int BASIC_PREMIUM= 5000;
	public static final String HYPERTENSION = "hyperTension";
	public static final String  BLOODPRESURE ="BloodPressure";
	public static final String  BLOODSUGAR ="BloodSugar";
	public static final String  DRUGS ="Drugs";
	public static final String  OVERWEIGHT ="OverWeight";
	public static final String  SMOKING ="Smoking";
	public static final String  ALCOHAL ="Alcohal";
	public static final String  EXERCISE ="exercise";

	public static void main(String[] args) {
	 
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter the Client Details");
		Client c = new Client();
		
		System.out.println("Enter the Name of the Client");
		c.setName(in.nextLine());
		
		System.out.println("Enter the age of Client");
		String age = in.nextLine();
		
		c.setAge(Short.valueOf(age));
		
		System.out.println("Enter the Gender of Client");
		c.setGender(in.nextLine());
		
		System.out.println("Enter Current healthConditions");
		System.out.println("enter yes or no");
		
		String boolintializer;
		Map healthMap = c.getHealth();		
		
		System.out.print("Does the  client has  Hypertension ?:");
	    healthMap.put(HYPERTENSION, in.nextLine());
		
	    System.out.print("Does the  client has  BloodPressure ?:");
	    healthMap.put(BLOODPRESURE, in.nextLine());
		
	    System.out.print("Does the  client has  BloodSugar ?:");
	    healthMap.put(BLOODSUGAR, in.nextLine());
		
	    System.out.print("Does the  client is OverWeigth ? :");
	    healthMap.put(OVERWEIGHT, in.nextLine());
		
	    Map habitsMap = c.getHabits();	
	    
	    
	    System.out.print("Does the  client Smoke ?:");
	    habitsMap.put( SMOKING, in.nextLine());
	    
	    System.out.print("Does the  client consume Alcohol ?:");
	    habitsMap.put(ALCOHAL, in.nextLine());
	    
	    
	    System.out.print("Does the  client do daily execerise ?:");
	    habitsMap.put( EXERCISE, in.nextLine());
	    
	    
	    
	    System.out.print("Does the  client consume drugs ?:");
	    habitsMap.put(DRUGS, in.nextLine());
	   
	   
	    InsuranceQuoter iq = new InsuranceQuoter();
		
		iq.calculateAgePremium(c);
		iq.calculateGenderPremium(c);
		iq.calculateHabitsPremium(c);
		iq.calculateHealthPremium(c);
		
		System.out.println("premium is : " +c.getPremium());
	}
	
	
	 

	public void calculateAgePremium (Client client) {
		// age > 40  20%  increment 20 % for every 5 years
		// age 18 - 40  10% increment 10 % for every 5 years 
		// age below 18 0%
		
		
		float premium = client.getPremium();
		int ageFactor= client.getAge()/5;
		ageFactor = ageFactor -3;
		if(client.getAge() > 40) {
			premium  =  (premium * 20/100) * ageFactor + premium   ;
		}
		else
			if(client.getAge() < 40  && client.getAge() > 18 ){
				premium  =  (premium * 10 /100) * ageFactor + premium ;
			}
		
 
		client.setPremium(premium);
	}
	
public 	void  calculateGenderPremium(Client client) {
		if("Male".equalsIgnoreCase(client.getGender())){
			float premium = client.getPremium();
			premium  =  ( (premium * 2) /100 )  + premium  ;
			client.setPremium(premium);

		}
	}
	
	
	
	
 void  calculateHealthPremium(Client client) {
	Map health = client.getHealth();
	float premium = client.getPremium();
	 
    if( "yes".equalsIgnoreCase(health.get(HYPERTENSION).toString()))
    	{
    	premium +=  (premium * 1) /100;
    	}
    if( "yes".equalsIgnoreCase(health.get(BLOODPRESURE).toString()))
	{
	premium +=  (premium * 1 )/100 ;
	}
    
    if( "yes".equalsIgnoreCase(health.get(BLOODSUGAR).toString()))
   	{
   	premium +=  (premium * 1)  /100;
   	}
    if( "yes".equalsIgnoreCase(health.get(OVERWEIGHT).toString()))
   	{
   	premium +=  (premium * 1) /100;
   	} 
    
    client.setPremium(premium);
    
 }
 
 void calculateHabitsPremium(Client client) {
		Map habits = client.getHabits();
		float premium = client.getPremium();
	 if( "yes".equalsIgnoreCase(habits.get( EXERCISE).toString()))
 	{
		 premium -=  (premium * 3) /100 ;
 	}
	 if( "yes".equalsIgnoreCase(habits.get( SMOKING).toString()))
	   	{
	   	premium +=  (premium * 3 )/100;
	   	} 
	 if( "yes".equalsIgnoreCase(habits.get(ALCOHAL).toString()))
	   	{
	   	premium +=  (premium * 3) /100;
	   	} 
	 
	 if( "yes".equalsIgnoreCase(habits.get(DRUGS).toString()))
	   	{
	   	premium +=  (premium * 3 ) /100;
	   	} 
	   
	 client.setPremium(premium);
 }
}

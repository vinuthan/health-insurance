package com.insurance.test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.insurance.model.Client;
 
import com.insurance.quoter.InsuranceQuoter;

public class test {

	public static final int BASIC_PREMIUM= 5000;
	public static final String HYPERTENSION = "hyperTension";
	public static final String  BLOODPRESURE ="BloodPressure";
	public static final String  BLOODSUGAR ="BloodSugar";
	public static final String  DRUGS ="Drugs";
	public static final String  OVERWEIGHT ="OverWeight";
	public static final String  SMOKING ="Smoking";
	public static final String  ALCOHAL ="Alcohal";
	public static final String  EXERCISE ="exercise";
    public static final String YES="YES";
    public static final String NO="NO";
    
	private static Client client = new Client();

	
	@Before
	public void setUp() throws Exception {
		 
		client.setName("VINUTHAN");
		client.setAge(35);
		client.setGender("MALE");
		HashMap<String, String> health= new HashMap<String, String> ();
		 health.put(HYPERTENSION, YES);
		 health.put(BLOODPRESURE, YES);
		 health.put(BLOODPRESURE, YES);
		 health.put(BLOODSUGAR, YES);
		 
		
		client.setHealth(health); 
		
		Map<String, String> habits = new HashMap<String, String> ();
		habits.put(SMOKING, YES);
		habits.put(EXERCISE, YES);
		habits.put(DRUGS, YES);
		habits.put(ALCOHAL,YES);
		client.setHabits(habits);
	}

   
	@Test
	public void testBasicPremiumAgeCondition() {
		client.setAge(17);
		HashMap<String, String> health= new HashMap<String, String> ();

	    InsuranceQuoter iq = new InsuranceQuoter();
		float basic  =  (int)BASIC_PREMIUM;
			iq.calculateAgePremium(client);
			int premium =  (int) client.getPremium();
			 assertEquals( basic,  client.getPremium(),0.5);
	 
	}
	
	@Test
	public void testAgeAbove18Condition() {
		client.setAge(38);
		HashMap<String, String> health= new HashMap<String, String> ();

	    InsuranceQuoter iq = new InsuranceQuoter();
		iq.calculateAgePremium(client);
		 	 assertEquals( 7140.0,  client.getPremium(),0.5);
	}
	
	@Test
	public void testAgeAbove40Condition() {
		client.setAge(44);
		HashMap<String, String> health= new HashMap<String, String> ();

	    InsuranceQuoter iq = new InsuranceQuoter();
	 
			iq.calculateAgePremium(client);
			 
			 assertEquals( 14280.0,  client.getPremium(),0.5);
	}
	
	@Test
	public void testGenderMaleCondition() {
		client.setAge(17);
		HashMap<String, String> health= new HashMap<String, String> ();

	    InsuranceQuoter iq = new InsuranceQuoter();
	 
			iq.calculateGenderPremium(client);
			 
			 assertEquals( 5100.0,  client.getPremium(),0.5);
	}
	
	@Test
	public void testGenderFemaleCondition() {
		client.setAge(17);
		client.setGender("FEMALE");
		HashMap<String, String> health= new HashMap<String, String> ();

	    InsuranceQuoter iq = new InsuranceQuoter();
	 
			iq.calculateGenderPremium(client);
			 
			 assertEquals( 5000.0,  client.getPremium(),0.5);
	}
}